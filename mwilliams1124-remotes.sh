#!/bin/bash

git remote rm origin
git remote rm upstream
git remote add origin git@gitlab.com:mwilliams1124/qmk_firmware.git
git remote add upstream https://github.com/qmk/qmk_firmware.git
